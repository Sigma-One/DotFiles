#!/bin/sh

# NVidia GPU stats for polybar
# Based off of the system-nvidia-smi script in the polybar-scripts repository (https://github.com/x70b1/polybar-scripts)


usage=$(nvidia-smi --query-gpu=utilization.gpu --format=csv,noheader,nounits)
temperature=$(nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader,nounits)

temp_ico=""

if [[ temperature -gt 20 ]]; then
  temp_ico=""
fi
if [[ temperature -gt 35 ]]; then
  temp_ico=""
fi
if [[ temperature -gt 50 ]]; then
  temp_ico=""
fi

output=$(echo "$usage" |awk '{ printf("  % 2s% ", $1)}')
output+=" $temp_ico"
output+=$(echo "$temperature" |awk '{ printf(" % 2s°C ", $1)}')

echo "$output"
