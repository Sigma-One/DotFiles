# DotFiles

Different config files, or "dotfiles" from my system.

Currently in this repo are configs and themes for:
* i3-gaps
* polybar
* oh-my-zsh
