colors=("202" "124" "202")

function collapse_pwd {
    echo $(pwd | sed -e "s,^$HOME,~,")
}

bold=$(tput bold)
norm=$(tput sgr0)

function generate_pwd {
	pwd_var="${$(pwd)/$HOME/~}"
	if [[ "$pwd_var" = "/" ]]
	then
		pwd_var="${pwd_var/\//ROOT}"
	else
	        firstletter="${pwd_var:0:1}"
		if [[ "$firstletter" = "/" ]]
		then
			pwd_var="${pwd_var/\//ROOT/}"
		fi
	fi
	pathvar=""
        index=1
	cindex=1
        IFS='/' read -r -A array <<< "$pwd_var"

        for i in "${array[@]}"
        do
                pathvar=$pathvar"%F{0} $i "
		if [[ $index == $((${#array[@]})) ]]
		then
			pathvar=$pathvar"%F{${colors[$cindex]}}%k%F{0}"
		else
			pathvar=$pathvar"%K{${colors[$(($cindex + 1))]}}%F{${colors[$cindex]}}%F{0}"
		fi
		index=$(($index + 1))
		if [[ $cindex == 1 ]]
		then
			cindex=2
		else
			cindex=1
		fi
        done
	echo "$pathvar"
}


setopt PROMPT_SUBST

ICO="%F{1} "

if [[ $UID == "0" ]]
then
    ICO="%F{83} "
fi

PROMPT='%K{202}$(generate_pwd) %f%k%F{1}%B%*  %F{202}%n%F{160} %b%B %F{202}%m%f  %b$ICO%F{1}%B  %f%b : '
